<!DOCTYPE html>
<html lang="en">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Banyan Catering</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <style>
        .b{
      background-image: url("background1.jpg");
      background-repeat: none;
      background-attachment: fixed;
  }
        </style>
    </head>

    <body class="b">
        <!--the top namvigation bar-->
        <nav class="orange n" role="navigation">
            <div class="nav-wrapper container">
            <a id="logo-container" href="#" class="brand-logo">Products</a>
            <ul class="right hide-on-med-and-down">
                <li><a href="products">Our Products</a></li>
                <li><a href="manager">Manager</a></li>
                <li><a href="order">Make an Order</a></li>
                <li><a href="searchfunc">Search</a></li>
            </ul>

            <!--the side navigation bar that only pop ups on small screens-->
            <ul id="nav-mobile" class="sidenav">
                <li><a href="products">Our Products</a></li>
                <li><a href="manager">Manager</a></li>
                <li><a href="order">Make an Order</a></li>        
                <li><a href="searchfunc">Search</a></li>
            </ul>
            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            </div>
        </nav>


        <h3 class="center white-text">Products</h3>

            <div class="container">

                <div class="row">
                    @foreach ($products as $products)

                        <div class="col 3">
                            <div class="card" style="width: 18rem;">
                                <div class="card-image">
                                    <img src="best.jpg" alt="">
                                </div>
                                <div class="card-content">
                                    <span class="card-title">{{$products->name}}</span>
                                    <p><strong>Price:</strong> ${{$products->cost}}</p>
                                </div>
                                <div class="card-action">
                                    <a href="{{url('add-to-cart/'.$products->id) }}" class ="waves-effect waves-light btn-small indigo">Add to cart</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        <!-- Scripts-->
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="js/materialize.js"></script>
        <script src="js/init.js"></script>

    </body>
</html>
