<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Banyan Catering</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <style>
      .b{
          background-image: url("background1.jpg");
          background-repeat: none;
          
      }
      #index-banner{
          float: center;
      }
      .t{
        color: white;
      }
      .c {
        margin: 0 auto;
        max-width: 30%;
        width: 90%;
      }
      @media only screen and (min-width: 601px) {
        .c {
          width: 30%;
        }
      }
      @media only screen and (min-width: 993px) {
        .c {
          width: 30%;
        }
      }
      
  </style>
     <!--the top namvigation bar-->
  <nav class="orange n" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo">Receipt</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="products">Our Products</a></li>
        <li><a href="manager">Manager</a></li>
        <li><a href="order">Make an Order</a></li>
        <li><a href="searchfunc">Search</a></li>
      </ul>

      <!--the side navigation bar that only pop ups on small screens-->
      <ul id="nav-mobile" class="sidenav">
        <li><a href="products">Our Products</a></li>
        <li><a href="manager">Manager</a></li>
        <li><a href="order">Make an Order</a></li>        
        <li><a href="searchfunc">Search</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  
</head>

<body class = "b">
    

    <div class="container t center">
        <br><br>
        <h2>Your Purchase</h2>
        <br><br>
        <h4>Item : {{ $item }} </h4>    
        <h4>Quantity : {{ $quantity }} </h4>
        <h4>Total Price : ${{ $t_cost }} </h4>
        <h4>Date and Time: {{ $time }} </h4>
        <br><br>
    </div>

    
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if(exist){
      alert(msg);
    }
  </script>

  </body>
</html>
