<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Banyan Catering</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <style>
      .b{
          background-image: url("background1.jpg");
          background-size: cover;
          background-repeat: none;
      }
      #index-banner{
          float: center;
      }
  </style>

</head>

<body class = "b">
  <!-- the top namvigation bar-->
  <nav class="orange n" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo">Manager</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="products">Our Products</a></li>
        <li><a href="manager">Manager</a></li>
        <li><a href="order">Make an Order</a></li>
        <li><a href="searchfunc">Search</a></li>
      </ul>

      <!-- the side navigation bar that only pop ups on small screens -->
      <ul id="nav-mobile" class="sidenav">
        <li><a href="products">Our Products</a></li>
        <li><a href="manager">Manager</a></li>
        <li><a href="order">Make an Order</a></li>        
        <li><a href="searchfunc">Search</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>



  <div id="index-banner" class="container c">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center orange-text text-lighten-2">Dashboard</h1>
        <br><br>
        <!--this shows buttons with that the manager can use to interact with the database-->
        <div class="row center">
            <div class="col s12 m6 l4">
                <a href="create" id="download-button" class="btn-large waves-effect waves-light orange darken-1">Add new menu item</a>
            </div>  
            <div class="col s12 m6 l4">
                <a href="allItems" id="download-button" class="btn-large waves-effect waves-light orange darken-1">View all items</a>
            </div> 
        
            <div class="col s12 m6 l4">
                <a href="edit" id="download-button" class="btn-large waves-effect waves-light orange darken-1">Update Inventory</a>
            </div>    
        </div>

      
        <br><br>
      </div>
    </div>
  </div>

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
