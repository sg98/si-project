<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Banyan Catering</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <style>
      .b{
          background-image: url("background1.jpg");
          background-repeat: none;
      }
      #index-banner{
          float: center;
      }
      .c {
        margin: 0 auto;
        max-width: 30%;
        width: 90%;
      }
      @media only screen and (min-width: 601px) {
        .c {
          width: 30%;
        }
      }
      @media only screen and (min-width: 993px) {
        .c {
          width: 30%;
        }
      }
  </style>

</head>

<body class = "b">
  <nav class="orange n" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo">Updating Records</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="products">Our Products</a></li>
        <li><a href="manager">Manager</a></li>
        <li><a href="order">Make an Order</a></li>
        <li><a href="searchfunc">Search</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="products">Our Products</a></li>
        <li><a href="manager">Manager</a></li>
        <li><a href="order">Make an Order</a></li>        
        <li><a href="searchfunc">Search</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>

<section class = "container c grey-text" width = "50%">
    <h2 style = "color:orange;" class = "center">Add an Item</h2>
    <form style = "color:orange;" class = "transparent" action="edit" method="POST">
      @csrf

        <label class="orange-text">ID of Menu Item: </label>
        <input style = "color:orange;" type="text" name="id" value="">

        <label class="orange-text">Name of Menu Item: </label>
        <input style = "color:orange;" type="text" name="name" value="">

        <label class="orange-text">Cost: </label>
        <input style = "color:orange;" type="text" name="cost" value="">

        <label class="orange-text">Initial Stock: </label>
        <input style = "color:orange;" type="text" name="stock" value="">

        <div class ="center">
            <input type="submit" name="submit" value="Update Item" class ="btn brand z-depth-0 orange darken-1">
        </div>
    </form>
</section>

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script>
    $(document).ready(function(){
    $('select').formSelect();
  });
  </script>

  </body>
</html>
