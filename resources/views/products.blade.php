<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Menu</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <style>
      .b{
          background-image: url("background1.jpg");
          background-repeat: none;
          
      }
      #index-banner{
          float: center;
      }
      .card {
        border-radius: 25px;
        float:left;
        margin: 1.5%;
        width: 30%;
      }
      
      .t{
        background-color: rgba(255,255,255,0.3);
      }
      }.wrapper {
        max-width: 1140px;
        padding: 10px;
        margin: 10px auto;
        vertical-align: middle;
        background: blurred;
      }.product-index h1 {
        margin-bottom: 10px;
      }

      .product-index .product-item {
        margin: 10px 0;
        padding: 10px;
        vertical-align: middle;
        background: #f4f4f4;
      }

      .product-index .product-item img {
        
        vertical-align: sub;
        display: inline-block;
      }

      .product-index .product-item h4 {
        display: inline-block;
        font-weight: normal;
        margin-left: 20px;
      }

      .product-index .product-item h4 a {
        color: #777;
        text-decoration: none !important;
      }
  </style>

</head>

<body class = "b">
  <!--the top namvigation bar-->
  <nav class="orange n" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo">Products</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="products">Our Products</a></li>
        <li><a href="manager">Manager</a></li>
        <li><a href="order">Make an Order</a></li>
        <li><a href="searchfunc">Search</a></li>
      </ul>

      <!--the side navigation bar that only pop ups on small screens-->
      <ul id="nav-mobile" class="sidenav">
        <li><a href="products">Our Products</a></li>
        <li><a href="manager">Manager</a></li>
        <li><a href="order">Make an Order</a></li>        
        <li><a href="searchfunc">Search</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>


 <!--table displaying all items in the products table of the database-->
<section class = "container align-center">
  <br>
    <h2 class = "center " style = "color:orange;">Products</h2>
    <br><br>
    <div class="wrapper t z-depth-4 product-index">          
      <table style = "color:black;" class="table striped">
        <thead >
          <tr>
            <th>Name</th>
            <th>Cost</th>
            <th>Stock</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($products as $prod)
          <tr>
            <td> {{ $prod->name }}</td>
            <td> {{ $prod->cost }}</td>
            <td> {{ $prod->stock }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    
</section>

<!--this section prints the error message -->
<div class="container">
  <p class="message center orange-text">{{ session('message')}}</p>
</div>

  
  <!--  Scripts-->
  
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script>
      $(document).ready(function(){
      $('.materialboxed').materialbox();
      });
  </script>

  </body>
</html>
