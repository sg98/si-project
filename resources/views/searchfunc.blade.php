<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Banyan Catering</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <style>
      .b{
          background-image: url("background1.jpg");
          background-repeat: none;
          text-decoration-color: orange;
      }
      #index-banner{
          float: center;
      }
      .card {
        border-radius: 25px;
        float:left;
        margin: 1.5%;
        width: 30%;
      }
      .card-image{
        border-top-right-radius: 25px;
        border-top-left-radius: 25px;
      }
  </style>

</head>

<body class = "b">
  <!-- the top namvigation bar-->
  <nav class="orange n" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo">Search</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="products">Our Products</a></li>
        <li><a href="manager">Manager</a></li>
        <li><a href="order">Make an Order</a></li>
        <li><a href="searchfunc">Search</a></li>
      </ul>

      <!-- the side navigation bar that only pop ups on small screens -->
      <ul id="nav-mobile" class="sidenav">
        <li><a href="products">Our Products</a></li>
        <li><a href="manager">Manager</a></li>
        <li><a href="order">Make an Order</a></li>        
        <li><a href="searchfunc">Search</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>

  <section class = "container align-center">
    <h4 class = "center" style = "color:orange;">All Items</h4>
    <br><br><br>
    <div class="wrapper Search">
    </div>
  </section>

   <!-- takes the name of the product that the customer is looking for -->
  <div class=" container form-group">
    <form action="/searchfunc" method="get" >
        <br>
        <!-- sends the name to the search function and performs a search of the database-->

        <input style = "color:orange;" type='search' name="q" value="" placeholder="  Enter the Name of the item ">
        <button type ="submit" class="btn btn-default orange darken-2">Search</button>
        <br>
    </form>

    @if(isset($details))
      <div>
        <!-- the results of the search are displayed here is table form -->

        <h4 style = "color:orange;">Results</h4>
        <table style = "color:orange;" class="table striped">
          <thead >
            <tr>
              <th>Item Number</th>
              <th>Name</th>
              <th>Cost</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($details as $item)
            <tr>
              <td style = "color:black;"> {{ $item->id }}</td>
              <td style = "color:black;"> {{ $item->name }}</td>
              <td style = "color:black;"> {{ $item->cost }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      <!--below displays an error message if the item is not found-->
      @elseif(isset($message))
    <p style = "color:orange;"> {{ $message }}</p>
    @endif
      
  </div>
 
  <!--  Scripts
  //handles javascript functions such as the dropdowns and sidenav bar-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
