<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Banyan Catering</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <style>
      .b{
          background-image: url("background1.jpg");
          background-repeat: none;
          
      }
      #index-banner{
          float: center;
      }
      .card {
        border-radius: 25px;
        float:left;
        margin: 1.5%;
        width: 30%;
      }
      
      .t{
        background-color: rgba(255,255,255,0.3);
      }
      }.wrapper {
        max-width: 1140px;
        padding: 10px;
        margin: 10px auto;
        vertical-align: middle;
        background: blurred;
      }.product-index h1 {
        margin-bottom: 10px;
      }

      .product-index .product-item {
        margin: 10px 0;
        padding: 10px;
        vertical-align: middle;
        background: #f4f4f4;
      }

      .product-index .product-item img {
        
        vertical-align: sub;
        display: inline-block;
      }

      .product-index .product-item h4 {
        display: inline-block;
        font-weight: normal;
        margin-left: 20px;
      }

      .product-index .product-item h4 a {
        color: #777;
        text-decoration: none !important;
      }
  </style>

</head>

<body class = "b">
  <!-- the top namvigation bar-->
  <nav class="orange n" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo">Menu</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="products">Our Products</a></li>
        <li><a href="manager">Manager</a></li>
        <li><a href="order">Make an Order</a></li>
        <li><a href="searchfunc">Search</a></li>
      </ul>

      <!-- the side navigation bar that only pop ups on small screens -->
      <ul id="nav-mobile" class="sidenav">
        <li><a href="products">Our Products</a></li>
        <li><a href="manager">Manager</a></li>
        <li><a href="order">Make an Order</a></li>        
        <li><a href="searchfunc">Search</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>


 
<section class = "container align-center">
    <h2 class = "center"style = "color:orange;">All Items</h2>
    <br><br><br>
    <div class="wrapper t z-depth-4 product-index">          
      <table style = "color:black;" class="table striped">
        <thead >
          <tr>
            <th>Item Number</th>
            <th>Name</th>
            <th>In Stock</th>
            <th>Cost</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($products as $prod)
          <tr>
            <td> {{ $prod->id }}</td>
            <td> {{ $prod->name }}</td>
            <td> {{ $prod->stock }}</td>
            <td> {{ $prod->cost }}</td>
            <td>
            <form action="allItems/{{ $prod->id }}" method="POST">
              @csrf
              @method('DELETE')
              <button class=" btn orange darken-2">Delete</button>
            </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    
</section>

<div class="container">
  <p class="message center orange-text">{{ session('message')}}</p>
</div>

  

  <!--
  <footer class="page-footer orange">
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="brown-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>
-->

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script>
    $(document).ready(function(){
    $('.materialboxed').materialbox();
    });
  </script>

  </body>
</html>
