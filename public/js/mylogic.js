        $(".update-item").click(function (e) {
            console.log("I was clicked 1");
            e.preventDefault();
            var ele = $(this);
          //  var id=ele.attr("data-id");
            $.ajax({
                url: "update-cart",
                method: "patch",
                data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), stock: ele.parents("tr").find(".stock").val()},
                success: function (response) {
                    console.log(response);
                    alert("iii");

                    window.location.reload();
                },
                error:function(err){
                    console.error(err);
                }
            });
        });

        $(".delete-item").click(function (e) {
            console.log("I was clicked 2");
            e.preventDefault();

            var ele = $(this);
                $.ajax({
                    url: "remove-from-cart",
                    method: "delete",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        console.log(response);
                    alert("iii");
                        window.location.reload();
                    },
                    error:function(err){
                        console.error(err);
                    }
                });
        });
