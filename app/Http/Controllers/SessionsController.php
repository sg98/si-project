<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use Cart;
use DB;

class SessionsController extends Controller
{
    //

    public function create()
    {
        return view('signin');
    }

    public function store1(Request $request)
    {
        if( auth()-> attempt(request(['firstname','password'])) == false)
        {
            return back()->withErrors([
                'message' => 'the username or password is incorrect please try again' 
            ]);
        }

        $userID = 1;
        Cart::clear();
        Cart::session($userID)->clear();
        return redirect('products')->with('alert', 'Purchase Successful!');
    }

    public function destroy()
    {
        auth()->logout();

        return redirect('products');
    }

    public function postSignup(Request $r)
    {
        $this->validate($r,[
            'firstname' => 'required|unique:users',
            'lastname' => 'required',
            'password' => 'required|min:8'
        ]);

        $user = new User();
        $user->firstname = request('firstname');
        $user->lastname = request('lastname');
        $user->password = bcrypt(request('password'));
        $user->save();
        
        auth()->login($user);
        
        return redirect('products');
    }
}
