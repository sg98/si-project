<?php

namespace App\Http\Controllers;
use App\User;
use Auth;
use App\Products;
use App\Sales;
use DB;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // gets the data  from the products table and sends it to the all Items page to be displayed
    public function disp()
    {
        $products = Products::all();
        //$products = Products::orderBy('name', 'category')->get();
        //$products = Products::where('name', 'coffee')->get();
        //$products = Products::latest();

        return view('allItems',[
            'products' => $products,
        ]);
    }

     // gets the data  from the products table and sends it to the products page to be displayed
    public function display()
    {
        $products = Products::all();
        //$products = Products::orderBy('name', 'category')->get();
        //$products = Products::where('name', 'coffee')->get();
        //$products = Products::latest();

        return view('products',[
            'products' => $products,
        ]);
    }
    
    // this function performs the search of the database for the item specified by the user
    public function search(Request $r)
    {
        $q = $r->get('q');
        if(!empty($q)){
            $products = Products::where('name', 'LIKE', '%' . $q . '%' )->get();
            if(count($products) > 0){
                return view('searchfunc')->withDetails($products)->withQuery ($q);
            }
        }
        return view ('searchfunc')->withMessage('No Details found. Try to search again !');       
        
    }

// this function updates the record in the database
    public function update(Request $r)
    {
        $id = $r->get('id');
        $products = Products::find($id);
        $products->name = request('name');
        $products->cost = request('cost');
        $products->stock = request('stock');
        $products->save();

        return redirect('allItems')->with('message', 'Item Successfully added/updated to database');
    }

    public function create()
    {
        return view ('create');
    }

    //this function adds new items to the database
    public function store()
    {
        $products = new Products();
        $products->name = request('name');
        $products->cost = request('cost');
        $products->stock = request('stock');

        $products->save();

        return redirect('allItems')->with('message', 'Item Successfully added/updated to database');
    }

    //this allows the manager to remove an item form the database

    public function destroy($id)
    {
        $products = Products::findOrFail($id);
        $products->delete();
        return redirect('allItems');
    }

    //handles orders made by user
    //this will update the databse with the stock after an order was made
    public function order(Request $request)
    {
        $item = $request->get('name');

        $data = DB::table('sales')->get();
        $quantity = $request->get('stock');
        
        $cost = DB::table('products')->where('name', $item)->value('cost');
        $time = DB::table('products')->where('name', $item)->value('updated_at');
        $t_cost = $cost * $quantity;

        DB::table('sales')->insert([
            'name' => $item,
            'quantity' => $quantity,
            'sale' => $t_cost
        ]);

        Products::where('name', $item)->decrement('stock', $quantity);
        
        return view('receipt', compact('item', 'quantity', 't_cost', 'time'));
    }
}