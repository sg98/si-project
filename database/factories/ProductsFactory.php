<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Products;
use Faker\Generator as Faker;

$factory->define(Products::class, function (Faker $faker) {
    return [
        'name'=> $faker->name,
        'stock'=> $faker-> stock,
        'cost'=> $faker-> cost,
        'category'=> $faker->beverage,
    ];
});
