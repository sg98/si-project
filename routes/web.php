<?php
use Illuminate\Http\Request;
use App\Products;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/manager', function () {
    return view('manager');
});
Route::get('/searchfunc', function(){
    return view('searchfunc');
});
Route::get('/edit', function(){
    return view('edit');
});
Route::get('/order', function(){
    return view('order');
});
Route::get('/products', function(){
    return view('products');
});
Route::get('/receipt', function () {
    return view('receipt');
});
Route::get('/allItems', 'ProductsController@disp');
Route::get('/searchfunc', 'ProductsController@search');
Route::post('/edit', 'ProductsController@update');
Route::post('/order', 'ProductsController@order');
Route::get('/create', 'ProductsController@create');
Route::post('/create', 'ProductsController@store');
Route::delete('/allItems/{id}', 'ProductsController@destroy');
Route::get('/report', 'ChartController@index');

Route::get('/products', 'ProductsController@display');